package com.aichamorro.dal.dataquery;

import com.aichamorro.dal.dataquery.adapters.SqlDataQueryAdapter;
import com.aichamorro.dal.dataquery.annotations.ModelName;
import com.aichamorro.dal.dataquery.annotations.ModelField;
import com.aichamorro.dal.dataquery.annotations.ModelId;

import static com.aichamorro.dal.dataquery.DataQueryStatementFactory.*;

import junit.framework.TestCase;
import junit.framework.Test;
import junit.framework.TestSuite;
import static org.mockito.Mockito.*;

public class SqlDataQueryAdapterTest extends TestCase{
	public SqlDataQueryAdapterTest( String suiteName ) {
		super( suiteName );
	}

	public Test suite() {
		return new TestSuite(SqlDataQueryAdapterTest.class);
	}
	
	SqlDataQueryAdapter adapter;
	Queryable<Long> object;
	String objectClass;
	
	public void setUp() {
		adapter = new SqlDataQueryAdapter();
		object = mock(Queryable.class);
			when(object.getId()).thenReturn(5L);
		objectClass = object.getClass().getSimpleName();
	}
	
	public void tearDown() {
		adapter = null;
		object = null;
		objectClass = null;
	}

	public void testSelectQueryWithNoFilterReturnsSelectAllStatement() {
		DataQueryFactory query = DataQueryFactory.select(object.getClass());
		
		assertEquals("SELECT * FROM " + objectClass, adapter.objectForQuery(query.createQuery())); 
	}
	
	public void testSelectQueryWithFilterReturnsSelectWithWhereStatement() {
		DataQuery query = DataQueryFactory.select(object.getClass()).where("id='5'").createQuery();
		
		assertEquals("SELECT * FROM " + objectClass + " WHERE id='5'", adapter.objectForQuery(query));
	}
	
	public void testSelectQueryWithTwoFiltersUsingAndConcatenation() {
		DataQuery query = DataQueryFactory.select(object.getClass()).where(and("id='5'", "name='Alberto'")).createQuery();	
		
		assertEquals("SELECT * FROM " + objectClass + " WHERE (id='5' AND name='Alberto')", adapter.objectForQuery(query));
	}
	
	public void testSelectQueryWithTwoFiltersUsingOrConcatenation() {
		DataQuery query = DataQueryFactory.select(object.getClass()).where(or("id='5'", "name='Alberto'")).createQuery();
		
		assertEquals("SELECT * FROM " + objectClass + " WHERE (id='5' OR name='Alberto')", adapter.objectForQuery(query));
	}
	
	public void testSelectQueryWithMoreThanTwoFiltersUsingSeveralConcatenators() {
		DataQuery query = DataQueryFactory.select(object.getClass()).where(or(and("id='5'", "name='Alberto'"),statement("surname='Chamorro'"))).createQuery();
		
		assertEquals("SELECT * FROM " + objectClass + " WHERE ((id='5' AND name='Alberto') OR surname='Chamorro')", adapter.objectForQuery(query));
	}
	
	public void testSelectQueryWithFilterGroupsNotInvolvingWhere() {
		DataQuery query = DataQueryFactory.select(object.getClass()).where(and(statement("id='5'"), or("name='Alberto'", "surname='Chamorro'"))).createQuery();
		
		assertEquals("SELECT * FROM " + objectClass + " WHERE (id='5' AND (name='Alberto' OR surname='Chamorro'))", adapter.objectForQuery(query));
	}
	
	public void testSelectQueryWithFilterGroups() {
		DataQuery query = DataQueryFactory.select(object.getClass()).where(or(and("id='5'", "name='Alberto'"), statement("surname='Chamorro'"))).createQuery();

		assertEquals("SELECT * FROM " + objectClass + " WHERE ((id='5' AND name='Alberto') OR surname='Chamorro')", adapter.objectForQuery(query));
	}
	
	public void testSimpleDeleteQuery() {
		DataQuery query = DataQueryFactory.delete(new MockModelForInsert()).createQuery();
		
		assertEquals("DELETE FROM Queryable WHERE idMock='5'", adapter.objectForQuery(query));
	}
	
	public void testSimpleInsertQuery() {
		DataQuery query = DataQueryFactory.insert(new MockModelForInsert()).createQuery();
		
		assertEquals("INSERT INTO Queryable(name,surname,age) VALUES('Alberto','Chamorro','32')", adapter.objectForQuery(query));
	}
	
	public void testUpdateSimpleQuery() {
		DataQuery query = DataQueryFactory.update(new MockModelForInsert()).createQuery();
		
		assertEquals("UPDATE Queryable SET name='Alberto',surname='Chamorro',age='32' WHERE idMock='5'", adapter.objectForQuery(query));
	}
}

@ModelName("Queryable")
class MockModelForInsert implements Queryable<Long> {
	@ModelField
	private String name;
	
	@ModelField("surname")
	private String _surname;
	
	@ModelField("age")
	private long _age;
	
	@ModelId("idMock")
	private Long _id;

	public MockModelForInsert() {
		name = "Alberto";
		_surname = "Chamorro";
		_age = 32;
		_id = Long.valueOf(5);
	}
	
	public Long getId() {
		return _id;
	}
}
