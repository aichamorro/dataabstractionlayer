package com.aichamorro.dal.dataquery;

public interface Queryable<T> {
	public T getId();
}
